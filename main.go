package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

var (
	port = 1729
	tmpf = "tmp.txt"
)

func handleMan(w http.ResponseWriter, r *http.Request) {
	manpage := strings.TrimPrefix(r.URL.Path, "/")

	cmd := exec.Command("bash", "-c", fmt.Sprintf("man %s > %s", manpage, tmpf))
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		log.Printf("main.go::handleMan::cmd.Run::ERROR: %s", err.Error())
	}

	f, err := os.Open(tmpf)
	if err != nil {
		log.Printf("main.go::handleMan::os.Open::ERROR: %s", err.Error())
	}
	defer f.Close()

	io.Copy(w, f)
}

func main() {
	http.HandleFunc("/", handleMan)
	log.Printf("Serving man pages at localhost:%d ...", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
