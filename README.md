---
title: MyMan
---

A webserver that serves man pages at `http://localhost:1729/:manpage` to make it possible to link to from markdown files.

## Example Usage

`make && ./myman`

`lynx http://localhost:1729/man`
